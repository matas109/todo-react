import React, {Component} from 'react';
import firebase from './../../config/firebase';
import {withRouter} from "react-router-dom";
import PropTypes from 'prop-types';
import './auth.scss'
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import LinearProgress from '@material-ui/core/LinearProgress';

class SignUp extends Component {
    state = {
        email: '',
        password: '',
        errors: {
            weakPassword: false,
            emailUsed: false,
            emailInvalid: false
        },
        isLoading: false
    };

    onChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value,
            errors: {
                weakPassword: false,
                emailUsed: false
            }
        });
    };

    onSubmit = (e) => {
        e.preventDefault();
        firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.password)
            .then(() => {
                this.setState({
                    isLoading: false
                });
                this.props.history.push('/');
            })
            .catch((error) => {
                console.log(error);
                if (error.code === "auth/email-already-in-use") {
                    this.setState({
                        errors: {
                            emailUsed: true
                        },
                        isLoading: false
                    });
                } else if (error.code === "auth/weak-password") {
                    this.setState({
                        errors: {
                            weakPassword: true
                        },
                        isLoading: false
                    });
                } else if (error.code === "auth/invalid-email") {
                    this.setState({
                        errors: {
                            emailInvalid: true
                        },
                        isLoading: false
                    });
                }
            });
        this.setState({
            isLoading: true
        });
    };

    render() {
        return (
            <div className="auth">
                <LinearProgress style={this.state.isLoading ? {display: 'block'} : {display: 'none'}}/>
                <div className="auth--wrapper">
                    <h1>Sign Up</h1>
                    <form onSubmit={this.onSubmit}>
                        <TextField
                            id="email"
                            label="Email"
                            type="email"
                            onChange={this.onChange}
                            margin="normal"
                            variant="outlined"
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                        <TextField
                            id="password"
                            label="Password"
                            type="password"
                            onChange={this.onChange}
                            margin="normal"
                            variant="outlined"
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                        <Button type="submit" variant="contained" color="primary">
                            Register
                        </Button>
                    </form>
                    <div className="auth--error" style={this.state.errors.emailUsed ? {display: "block"} : {display: "none"}}>Email already in use.</div>
                    <div className="auth--error" style={this.state.errors.weakPassword ? {display: "block"} : {display: "none"}}>Password should be at least 6 characters.</div>
                    <div className="auth--error" style={this.state.errors.emailInvalid ? {display: "block"} : {display: "none"}}>The email address is badly formatted.</div>
                </div>
            </div>
        );
    }
}

SignUp.propTypes = {
    history: PropTypes.object,
    location: PropTypes.object,
    match: PropTypes.object
};

export default withRouter(SignUp);
