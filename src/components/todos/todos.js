import React, {Component} from 'react';
import PropTypes from 'prop-types';
import TodoItem from "../todo-item/todo-item";

class Todos extends Component {
    render() {
        let tableHeadStyle = {
            backgroundColor: "#202124",
            color: "#fff",
            fontSize: "18px",
            padding: "15px 0 15px 15px",
        };
        return (
            <div data-test="todosWrapper">
                <div data-test="listHeader" style={tableHeadStyle}>
                    {this.props.user ? this.props.user.email : 'Your'} todo list
                </div>
                {this.props.todos && this.props.todos.map((todo, index) => (
                    <TodoItem i={index} key={todo.id} todo={todo} completeTodo={this.props.completeTodo}
                              deleteTodo={this.props.deleteTodo} editTodo={this.props.editTodo}/>
                ))}
            </div>
        );
    }
}

Todos.propTypes = {
    todos: PropTypes.array.isRequired,
    completeTodo: PropTypes.func,
    deleteTodo: PropTypes.func,
    editTodo: PropTypes.func,
    user: PropTypes.object
};

export default Todos;
