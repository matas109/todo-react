# Todo-react application

### Description

This TODO application was made with React and has all CRUD functionality.
This means all Todo's are displayed in a list. You can create, edit and delete todo's. 
Firebase database, authentication and hosting were used. SCSS files were used to upstyle web
application.


The site is hosted on 
https://todo-application-295ae.firebaseapp.com/ 

### Installing

```
npm install
```

Installs all the needed dependencies to run the project.

## Starting project

### `npm start`

Runs the app in the development mode.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Running the tests

### `npm test`

Launches the test runner in the interactive watch mode.

There are 4 tests written for todo's component.

## Building project for production

### `npm run build`

Builds the app for production to the `build` folder.

It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.

Your app is ready to be deployed!

