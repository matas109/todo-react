import firebase from 'firebase/app';
import 'firebase/firestore'
import 'firebase/auth'

const config = {
    apiKey: "AIzaSyCq73SjeIpRLGdpZxhG50c8cXLpawOCtJU",
    authDomain: "todo-application-295ae.firebaseapp.com",
    databaseURL: "https://todo-application-295ae.firebaseio.com",
    projectId: "todo-application-295ae",
    storageBucket: "todo-application-295ae.appspot.com",
    messagingSenderId: "881212558335"
};

firebase.initializeApp(config);
export default firebase;