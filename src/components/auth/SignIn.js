import React, {Component} from 'react';
import firebase from './../../config/firebase';
import {withRouter} from "react-router-dom";
import PropTypes from 'prop-types';
import './auth.scss'
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import LinearProgress from '@material-ui/core/LinearProgress';

class SignIn extends Component {
    state = {
        email: '',
        password: '',
        error: false,
        isLoading: false
    };

    onChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value,
            error: false
        });
    };

    onSubmit = (e) => {
        e.preventDefault();
        firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password)
            .then(() => {
                this.setState({
                    isLoading: false
                });
                this.props.history.push('/');
            })
            .catch((error) => {
                console.log(error);
                this.setState({
                    error: true,
                    isLoading: false
                });
            });
        this.setState({
            isLoading: true
        });
    };

    render() {
        return (
            <div className="auth">
                <LinearProgress style={this.state.isLoading ? {display: 'block'} : {display: 'none'}}/>
                <div className="auth--wrapper">
                    <h1>Sign In</h1>
                    <form onSubmit={this.onSubmit}>
                        <TextField
                            id="email"
                            label="Email"
                            type="email"
                            onChange={this.onChange}
                            margin="normal"
                            variant="outlined"
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                        <TextField
                            id="password"
                            label="Password"
                            type="password"
                            onChange={this.onChange}
                            margin="normal"
                            variant="outlined"
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                        <Button type="submit" variant="contained" color="primary">
                            Login
                        </Button>
                    </form>
                    <div className="auth--error" style={this.state.error ? {display: "block"} : {display: "none"}}>Email or username was incorect.</div>
                </div>
            </div>
        );
    }
}

SignIn.propTypes = {
    history: PropTypes.object,
    location: PropTypes.object,
    match: PropTypes.object
};

export default withRouter(SignIn);
