import React from 'react';
import {shallow, mount} from 'enzyme';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Todos from './todos';
import TodoItem from '../todo-item/todo-item';

Enzyme.configure({adapter: new Adapter()});

describe('Todos', () => {
    const todos = [
        {
            completed: false,
            id: "AigPRheZ4m14qrnKwrVK",
            title: "Etiam tempus, turpis vitae pretium laoreet",
        },
        {
            completed: false,
            id: "JJRh9MOnzaweFCaSJvT3",
            title: "Suspendisse diam erat"
        },
        {
            completed: false,
            id: "JJRh9MOnzawessaSJvT3",
            title: "Suspendisse diam erat"
        }
    ];

    const user = {
        null: null,
        notNull: {
            email: 'foo@bar.com',
            foo: 'bar'
        }
    };

    const completeTodo = () => {};
    const deleteTodo = () => {};
    const editTodo = () => {};

    it('If user is not logged I should see "Your todo list"', () => {
        const wrapper = shallow(<Todos todos={todos} user={user.null}/>);

        expect(wrapper.find('[data-test="listHeader"]').text()).toEqual('Your todo list')
    });

    it('If user is logged in I should see "USER-EMAIL todo list"', () => {
        const wrapper = shallow(<Todos todos={todos} user={user.notNull}/>);

        expect(wrapper.find('[data-test="listHeader"]').text()).toEqual(user.notNull.email + ' todo list')
    });

    it('Todos component has TodoItem child component', () => {
        const wrapper = shallow(<Todos todos={todos}/>);

        expect(wrapper.containsMatchingElement(<TodoItem/>)).toBeTruthy()
    });

    it('Display all todo elements in list', () => {
        const wrapper = mount(<Todos todos={todos}
                                     user={user.notNull}
                                     completeTodo={completeTodo}
                                     deleteTodo={deleteTodo}
                                     editTodo={editTodo}/>);


        expect(wrapper.find('[data-test="todosWrapper"]').children()).toHaveLength(todos.length + 1);
    });
});

test('True is equal to true', () => {
    expect(true).toBeTruthy();
});