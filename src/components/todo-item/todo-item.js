import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './todo-item.scss';
import editIcon from './../../assets/svg/edit.svg';
import deleteIcon from './../../assets/svg/delete.svg';
import checkIcon from './../../assets/svg/check.svg';
import clearIcon from './../../assets/svg/clear.svg';
import Checkbox from '@material-ui/core/Checkbox';

class TodoItem extends Component {
    state = {
        title: this.props.todo.title,
        oldTitle: this.props.todo.title,
        edit: false
    };

    onChange = (e) => this.setState({[e.target.name]: e.target.value});

    onSubmit = (e) => {
        e.preventDefault();
        this.props.editTodo(this.props.todo.id, this.state.title);
        this.setState({edit: !this.state.edit})
    };

    onEdit = (e) => {
        this.setState({edit: !this.state.edit});
        e.target.value = this.state.title;
    };

    cancelEditing = () => {
        this.setState({
            title: this.state.oldTitle,
            edit: !this.state.edit
        })
    };

    render() {
        const {id, title, completed} = this.props.todo;
        return (
            <div className={`todo-item ${(this.props.i % 2 === 0) ? '-gray' : ''}`}>
                <div className={`todo-item--left ${completed ? '-completed' : ''} ${this.state.edit ? '-edit' : ''}`}>
                    <Checkbox color="default" checked={completed} onChange={this.props.completeTodo.bind(this, id)}/>
                    <p onClick={!this.state.edit ? this.props.completeTodo.bind(this, id) : null}>{title}</p>
                    <form onSubmit={this.onSubmit}>
                        <input type="text"
                               name="title"
                               value={this.state.title}
                               onChange={this.onChange}
                        />
                    </form>
                </div>
                <div className="todo-item--right">
                    <div style={this.state.edit ? {display: "none"} : {display: "flex"}}>
                        <img src={editIcon} onClick={this.onEdit} alt="Edit"/>
                        <img src={deleteIcon} onClick={this.props.deleteTodo.bind(this, id)} alt="Delete"/>
                    </div>
                    <div style={this.state.edit ? {display: "flex"} : {display: "none"}}>
                        <img src={checkIcon} onClick={this.onSubmit} alt="Accept"/>
                        <img src={clearIcon} onClick={this.cancelEditing} alt="Cancel"/>
                    </div>
                </div>
            </div>
        );
    }
}

TodoItem.propTypes = {
    todo: PropTypes.object,
    completeTodo: PropTypes.func,
    deleteTodo: PropTypes.func,
    editTodo: PropTypes.func,
    i: PropTypes.number,
};

export default TodoItem;
