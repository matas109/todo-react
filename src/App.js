import React, {Component} from 'react';
import './App.scss';
import Todos from "./components/todos/todos";
import Header from "./components/header/header";
import AddTodo from "./components/add-todo/add-todo";
import {BrowserRouter, Switch, Route} from 'react-router-dom'
import firebase from './config/firebase';
import SignIn from "./components/auth/SignIn";
import SignUp from "./components/auth/SignUp";
import LinearProgress from '@material-ui/core/LinearProgress';
import FilterTodos from "./components/filter-todos/filter-todos";

class App extends Component {
    constructor() {
        super();
        this.state = {
            user: {},
            todos: [],
            filteredTodos: [],
            isLoading: false
        };
    }

    componentDidMount() {
        this.authListener();
        const db = firebase.firestore();
        db.collection('todos').get()
            .then((snapshot) => {
                this.setLoadingState();
                snapshot.forEach((doc) => {
                    const newTodo = {
                        id: doc.id,
                        title: doc.data().title,
                        completed: doc.data().completed
                    };
                    this.setState({todos: [...this.state.todos, newTodo]});
                });
            })
            .catch((err) => {
                console.log('Error getting documents', err);
            });
        this.setLoadingState();
    }

    completeTodo = (id) => {
        this.setState({
            todos: this.state.todos.map(todo => {
                if (todo.id === id) {
                    firebase.firestore().collection('todos').doc(id).update({
                        completed: !todo.completed
                    }).then(() => {
                        this.setLoadingState();
                    }).catch((err) => {
                        console.log('Error trying to complete document', err);
                    });
                    this.setLoadingState();
                    todo.completed = !todo.completed;
                }
                return todo;
            })
        })
    };

    deleteTodo = (id) => {
        firebase.firestore().collection('todos').doc(id).delete()
            .then(() => {
                this.setLoadingState();
            })
            .catch((err) => {
                console.log('Error deleting document', err);
            });
        this.setState({
            todos: [...this.state.todos.filter(todo => todo.id !== id)],
            isLoading: true
        });
    };

    addTodo = (title) => {
        firebase.firestore().collection('todos').add({
            title,
            completed: false
        }).then(ref => {
            let newTodo = {
                id: ref.id,
                title,
                completed: false
            };
            this.setState({
                    todos: [...this.state.todos, newTodo],
                    filteredTodos: [],
                    isLoading: false
                }
            );
        }).catch((err) => {
            console.log('Error adding document', err);
        });
        this.setLoadingState();
    };

    editTodo = (id, title) => {
        this.setState({
            todos: this.state.todos.map(todo => {
                if (todo.id === id) {
                    firebase.firestore().collection('todos').doc(id).update({
                        title: title
                    }).then(() => {
                        this.setLoadingState();
                    }).catch((err) => {
                        console.log('Error updating document', err);
                    });
                    this.setLoadingState();
                    todo.title = title;
                }
                return todo;
            })
        })
    };

    authListener() {
        firebase.auth().onAuthStateChanged((user) => {
            if (user) {
                this.setState({user});
            } else {
                this.setState({user: null});
            }
        });
    }

    logout = () => {
        firebase.auth().signOut()
            .then(() => {
                this.setLoadingState();
            })
            .catch((err) => {
                console.log('Error signing out', err);
            });
        this.setLoadingState();
    };

    setLoadingState = () => {
        this.setState({
            isLoading: !this.state.isLoading
        });
    };

    render() {
        return (
            <BrowserRouter>
                <div className="app">
                    <Header user={this.state.user} logout={this.logout} resetFiltered={() => this.setState({filteredTodos: []})}/>
                    <Switch>
                        <Route exact path="/" render={() => (
                            <div className="app--content">
                                <LinearProgress style={this.state.isLoading ? {display: 'block'} : {display: 'none'}}/>
                                <div className="app--content-wrapper">
                                    <AddTodo addTodo={this.addTodo}/>
                                    <FilterTodos todos={this.state.todos} resetFilteredTodos={this.state.filteredTodos} filteredTodos={(filteredTodos) => {
                                        this.setState({filteredTodos: filteredTodos})
                                    }}/>
                                    <Todos
                                        todos={(this.state.filteredTodos.length !== 0) ? this.state.filteredTodos : this.state.todos}
                                        user={this.state.user}
                                        completeTodo={this.completeTodo}
                                        deleteTodo={this.deleteTodo}
                                        editTodo={this.editTodo}/>
                                </div>
                            </div>
                        )}/>
                        <Route path="/signin" component={SignIn}/>
                        <Route path="/signup" component={SignUp}/>
                    </Switch>
                </div>
            </BrowserRouter>
        );
    }
}

export default App;
