import React, {Component} from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import './add-todo.scss';

class AddTodo extends Component {
    state = {
        title: '',
        error: false
    };

    onChange = (e) => this.setState({
        [e.target.id]: e.target.value,
        error: false
    });

    onSubmit = (e) => {
        e.preventDefault();
        if (this.state.title === '') {
            this.setState({error: true});
        } else {
            this.props.addTodo(this.state.title);
            this.setState({title: ''});
        }
    };

    render() {
        return (
            <div className="add-todo">
                <form onSubmit={this.onSubmit}>
                    <TextField
                        className="add-todo--form-field"
                        id="title"
                        label="Enter your todo here"
                        type="text"
                        value={this.state.title}
                        onChange={this.onChange}
                        variant="outlined"
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />
                    <div className="add-todo--form-button">
                        <Button type="submit" variant="outlined" color="secondary">
                            Add new todo
                        </Button>
                    </div>
                    <div className="add-todo--error"
                         style={this.state.error ? {display: "block"} : {display: "none"}}>Please enter your todo.
                    </div>
                </form>
            </div>
        );
    }
}

AddTodo.propTypes = {
    addTodo: PropTypes.func.isRequired
};

export default AddTodo;
