import React from 'react';
import './header.scss'
import {Link} from 'react-router-dom'
import PropTypes from 'prop-types';

const Header = (props) => {
    return (
        <div className="header">
            <div className="header--wrapper">
                <div className="header--logo">
                    <Link to="/" onClick={props.resetFiltered}>ToDo</Link>
                </div>
                {props.user ?
                    <div className="header--links">
                        <div>
                            <span onClick={() => {props.logout(); props.resetFiltered()}}>Logout</span>
                        </div>
                    </div> :
                    <div className="header--links">
                        <div>
                            <Link to="/signup" onClick={props.resetFiltered}>Signup</Link>
                        </div>
                        <div>
                            <Link to="/signin" onClick={props.resetFiltered}>Login</Link>
                        </div>
                    </div>}
            </div>
        </div>
    );
};

Header.propTypes = {
    user: PropTypes.object,
    logout: PropTypes.func.isRequired,
    resetFiltered: PropTypes.func.isRequired
};

export default Header;
