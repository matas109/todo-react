import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';

class FilterTodos extends Component {
    state = {
        filteredTodos: [],
        filteredByCompleted: false,
        filteredByNotCompleted: false
    };

    componentDidUpdate(prevProps) {
        if (prevProps.resetFilteredTodos !== this.props.resetFilteredTodos) {
            if (this.props.resetFilteredTodos.length === 0) {
                this.setState({
                    filteredByCompleted: false,
                    filteredByNotCompleted: false,
                })
            }
        }
    }

    filterByCompleted = () => {
        if (this.state.filteredByCompleted) {
            this.setState({
                    filteredTodos: [],
                    filteredByCompleted: false
                }, () => {
                    this.props.filteredTodos(this.state.filteredTodos);
                }
            );
        } else {
            this.setState({
                    filteredTodos: [...this.props.todos.filter(todo => todo.completed)],
                    filteredByCompleted: true,
                    filteredByNotCompleted: false
                }, () => {
                    this.props.filteredTodos(this.state.filteredTodos);
                }
            );
        }
    };

    filterByNotCompleted = () => {
        if (this.state.filteredByNotCompleted) {
            this.setState({
                    filteredTodos: [],
                    filteredByNotCompleted: false
                }, () => {
                    this.props.filteredTodos(this.state.filteredTodos);
                }
            );
        } else {
            this.setState({
                    filteredTodos: [...this.props.todos.filter(todo => !todo.completed)],
                    filteredByNotCompleted: true,
                    filteredByCompleted: false
                }, () => {
                    this.props.filteredTodos(this.state.filteredTodos);
                }
            );
        }
    };

    render() {
        return (
            <div style={{fontWeight: "bold", marginLeft: "10px"}}>
                Sort by:
                <Button onClick={this.filterByCompleted}>Completed</Button>
                <Button onClick={this.filterByNotCompleted}>Not completed</Button>
            </div>
        );
    }
}

FilterTodos.propTypes = {
    todos: PropTypes.array.isRequired,
    filteredTodos: PropTypes.func.isRequired,
    resetFilteredTodos: PropTypes.array,
};

export default FilterTodos;
